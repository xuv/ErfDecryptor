"""ERF file version enum."""

from enum import Enum


class Version(Enum):
	"""ERF file versions, value is the magic number as a string."""
	Erf20 = "ERF V2.0"
	Erf22 = "ERF V2.2"
	Erf30 = "ERF V3.0"
	# Other versions mentioned in ErfEditor.exe, details are unknown
	# Erf10
	# Erf25

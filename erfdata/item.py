from __future__ import annotations
"""Classes for loading and manipulating ERF file contents."""
"""
To add a new item version:
	1. Add an entry to version.Version.
	2. Create a subclass of ErfItem that:
		- Defines a table of contents header format.
		- Overrides __init__ to load that format.
		- Overrides get_bytes to serialize to that format.
	3. Add an entry to _version_map at the end of this file mapping the new
		Version to the new subclass.

If a new item version uses an identical table of contents header, just
add entries in Version and _version_map and point to the existing subclass.
"""

from struct import Struct

from erfdata.version import Version

import logging


_log = logging.getLogger(__name__)


class ErfItem:

	def __new__(cls, header: bytearray, version: Version) -> ErfItem:
		subclass = _version_map[version]
		return super().__new__(subclass)
	

	def __getnewargs__(self) -> tuple:
		return (None, self.version)


	@staticmethod
	def get_header_size(version: Version) -> int:
		subclass = _version_map[version]
		return subclass.header_size



class ErfItem20(ErfItem):

	_HEADER_FORMAT = '<64s I I'
	_header_struct = Struct(_HEADER_FORMAT)
	header_size = _header_struct.size
	version = Version.Erf20


	def __init__(self, header: bytearray, *args) -> None:
		super().__init__()
		self.name = header[:64].decode('utf-16-le')
		(
			self.offset,
			self.packed_size,
		) = self._header_struct.unpack(header)[1:]
		self.unpacked_size = self.packed_size
	

	def get_bytes(self) -> bytearray:
		return self._header_struct.pack(
			bytes(self.name, 'utf-16-le'),
			self.offset,
			self.packed_size,
		)


class ErfItem22(ErfItem):

	_HEADER_FORMAT = '<64s I I I'
	_header_struct = Struct(_HEADER_FORMAT)
	header_size = _header_struct.size
	version = Version.Erf22


	def __init__(self, header: bytearray, *args) -> None:
		super().__init__()
		self.name = header[:64].decode('utf-16-le')
		(
			self.offset,
			self.packed_size,
			self.unpacked_size,
		) = self._header_struct.unpack(header)[1:]
	

	def get_bytes(self) -> bytearray:
		return self._header_struct.pack(
			bytes(self.name, 'utf-16-le'),
			self.offset,
			self.packed_size,
			self.unpacked_size,
		)



class ErfItem30(ErfItem):

	_HEADER_FORMAT = '<I Q I I I I'
	_header_struct = Struct(_HEADER_FORMAT)
	header_size = _header_struct.size
	version = Version.Erf30


	def __init__(self, header: bytearray, *args) -> None:
		super().__init__()
		(
			self.name_offset,
			self.name_hash,
			self.type_hash,
			self.offset,
			self.packed_size,
			self.unpacked_size,
		) = self._header_struct.unpack(header)


	def get_bytes(self) -> bytearray:
		return self._header_struct.pack(
			self.name_offset,
			self.name_hash,
			self.type_hash,
			self.offset,
			self.packed_size,
			self.unpacked_size,
		)


_version_map = {
	Version.Erf20: ErfItem20,
	Version.Erf22: ErfItem22,
	Version.Erf30: ErfItem30,
}

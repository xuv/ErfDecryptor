from __future__ import annotations
"""Classes for loading and manipulating ERF files."""
"""
To add a new file version:
	1. Add an entry to version.Version.
	2. Create a subclass of ErfFile that:
		- Defines a header format.
		- Overrides load_header to load that format.
		- Overrides get_header_bytes to serialize to that format.
	3. Add an entry to _version_map at the end of this file mapping the new
		Version to the new subclass.
	4. Add an item.ErfItem for that Version (see that file for details).

This implementation is overkill (and inefficient) for a simple decryptor,
but can be readily built upon for other modding tools.
"""


from enum import Enum
from hashlib import md5
from struct import Struct
from io import IOBase

from erfdata.item import ErfItem
from erfdata.version import Version

import logging


_log = logging.getLogger(__name__)



class EncryptionScheme(Enum):
	NotEncrypted = 0
	XOR = 1
	# Both use Blowfish ECB but different key space and hashing scheme
	Blowfish_DAO = 2
	Blowfish_DA2 = 3



class ErfFile:

	@property
	def encryption_scheme(self) -> EncryptionScheme:
		return EncryptionScheme(self._flags >> 4 & 0xF)


	@encryption_scheme.setter
	def encryption_scheme(self, scheme: EncryptionScheme) -> None:
		# Clear encryption and protection flags
		self._flags &= 0xFFFF_FF0E
		if scheme != EncryptionScheme.NotEncrypted:
			# Set encryption flag
			self._flags |= scheme.value << 4
			# Set protection flag
			self._flags += 1


	@property
	def password(self) -> bytearray:
		return self._password


	@password.setter
	def password(self, new: str | int | bytearray) -> None:
		try:
			# Password is a 64-bit integer, aka 8 bytes
			self._password = int(new).to_bytes(8, 'little')
		except:
			self._password = new


	def __new__(cls, path: str | Version) -> ErfFile:
		if isinstance(path, Version):
			# hacky workaround for unpickling, necessary for multiprocessing
			version = path
		else:
			with open(path, 'rb') as file:
				version_str = Version(file.read(16).decode('utf-16-le'))
			try:
				version = Version(version_str)
			except:
				raise ValueError("File type not supported", version_str)

		try:
			subclass = _version_map[version]
		except:
			raise NotImplementedError(f"{ErfItem.__name__} for type", version)
		return object.__new__(subclass)
	

	def __getnewargs__(self) -> tuple:
		# other half of hacky workaround for unpickling
		return (self.version,)


	def __init__(self, path: str) -> None:
		"""Load an ERF file.
		
		path: Path to an ERF file.
		"""
		self._init_empty()
		self.path = path
		with open(self.path, 'rb') as file:
			file_bytes = file.read()
		
		self.load_header(file_bytes[:self.header_size])

		start_toc = self.header_size + self.stringtable_size
		end_toc = start_toc + self.file_count * ErfItem.get_header_size(self.version)

		self.load_stringtable(file_bytes[self.header_size:start_toc])
		self.load_toc(file_bytes[start_toc:end_toc])
		self.load_contents(file_bytes)
	

	def _init_empty(self) -> None:
		"""Initialize values that don't exist in some versions."""
		self.stringtable_size = 0
		self.year = 0
		self.day = 0
		self._flags = 0
		self.module_id = 0
		self.password_digest = b''
		self._unknown = 0xFFFF_FFFF


	def load_header(self, header: bytearray) -> None:
		"""Load basic ERF header from bytes.

		This does not load the stringtable or table of contents.
		
		header: Bytes of an ERF file header. Format must match this object's
		ERF version.
		"""
		raise NotImplementedError(f"{self.load_header.__name__} must be called on a specific version, not base class.")


	def load_stringtable(self, stringtable: bytearray) -> None:
		"""Load stringtable from bytes.
		
		stringtable: Bytes of a stringtable. Each entry is a 32-byte
		null-padded ASCII string.
		"""
		# TODO when properly implementing v3 files
		self.stringtable = stringtable


	def load_toc(self, toc: bytearray) -> None:
		"""Load table of contents from bytes.
		
		toc: Bytes of an array of ErfItem headers. Format must match this
		object's ERF version.
		"""
		header_size = ErfItem.get_header_size(self.version)
		if len(toc) % header_size != 0:
			raise ValueError("Table of contents is not a multiple of struct size", self.path)

		self.toc = []
		raw_entries = [toc[i:i+header_size] for i in range(0, len(toc), header_size)]
		for entry in raw_entries:
			self.toc.append(ErfItem(entry, self.version))
	

	def load_contents(self, file_bytes: bytearray) -> None:
		"""Load ERF contents from bytes.

		Contents are located according to the table of contents stored on
		this object.
		
		file_bytes: The full bytes of an ERF file, including header.
		"""
		self.contents = dict()
		for entry in self.toc:
			end = entry.offset + entry.packed_size
			self.contents[entry.offset] = file_bytes[entry.offset:end]
	

	def has_valid_password(self) -> bool:
		"""Return whether the password on this object matches its digest.
		
		Any password is considered invalid if the file is not encrypted.
		"""
		if self.encryption_scheme == EncryptionScheme.NotEncrypted:
			_log.warning("Checked password on unencrypted file")
			return False
		pw_int = int.from_bytes(self.password, 'little')
		return md5(bytes(str(pw_int), 'ascii')).digest() == self.password_digest


	def get_header_bytes(self, version: Version) -> bytearray:
		"""Convert ERF header to a specific version."""
		"""
		This is mostly out of curiosity.
		"""
		if not version:
			version = self.version
		version_map = {
			Version.Erf20: ErfFile20,
			Version.Erf22: ErfFile22,
			Version.Erf30: ErfFile30,
		}
		try:
			subclass = version_map[version]
		except:
			raise NotImplementedError
		return subclass.get_header_bytes(self)
	

	def write_bytes(self, file: IOBase, version: Version = None) -> None:
		"""Write complete ERF data to file.

		file: An open file object.

		version: Optional. A specific ERF version to write. If not provided,
		this object's current version is used.
		"""
		file.write(ErfFile.get_header_bytes(self, version))
		file.write(self.stringtable)
		for entry in self.toc:
			file.write(entry.get_bytes())
		for offset, data in self.contents.items():
			file.seek(offset)
			file.write(data)



class ErfFile20(ErfFile):

	HEADER_FORMAT = '<16s I I I I' # followed by toc_entry array
	_header_struct = Struct(HEADER_FORMAT)
	header_size = _header_struct.size


	def load_header(self, header: bytearray) -> None:
		self.version = Version(header[:16].decode('utf-16-le'))
		assert(self.version == Version.Erf20)

		( # magic number ignored via slice
			self.file_count,
			self.year,
			self.day,
			self._unknown,
		) = self._header_struct.unpack_from(header)[1:]


	def get_header_bytes(self) -> bytearray:
		return ErfFile20._header_struct.pack(
			bytes(Version.Erf20.value, 'utf-16-le'),
			self.file_count,
			self.year,
			self.day,
			self._unknown,
		)



class ErfFile22(ErfFile):

	HEADER_FORMAT = '<16s I I I I I I 16s' # followed by toc_entry array
	_header_struct = Struct(HEADER_FORMAT)
	header_size = _header_struct.size


	def load_header(self, header: bytearray) -> None:
		self.version = Version(header[:16].decode('utf-16-le'))
		assert(self.version == Version.Erf22)

		( # magic number ignored via slice
			self.file_count,
			self.year,
			self.day,
			self._unknown,
			self._flags,
			self.module_id,
			self.password_digest
		) = self._header_struct.unpack_from(header)[1:]

	
	def get_header_bytes(self) -> bytearray:
		return self._header_struct.pack(
			bytes(Version.Erf22.value, 'utf-16-le'),
			self.file_count,
			self.year,
			self.day,
			self._unknown,
			self._flags,
			self.module_id,
			self.password_digest,
		)



class ErfFile30(ErfFile):

	HEADER_FORMAT = '<16s I I I I 16s' # followed by stringtable, then toc_entry array
	_header_struct = Struct(HEADER_FORMAT)
	header_size = _header_struct.size


	def load_header(self, header: bytearray) -> None:
		self.version = Version(header[:16].decode('utf-16-le'))
		assert(self.version == Version.Erf30)

		( # magic number ignored via slice
			self.stringtable_size,
			self.file_count,
			self._flags,
			self.module_id,
			self.password_digest,
		) = self._header_struct.unpack_from(header)[1:]


	def get_header_bytes(self) -> bytearray:
		return self._header_struct.pack(
			bytes(Version.Erf30.value, 'utf-16-le'),
			self.stringtable_size,
			self.file_count,
			self._flags,
			self.module_id,
			self.password_digest,
		)



_version_map = {
	Version.Erf20: ErfFile20,
	Version.Erf22: ErfFile22,
	Version.Erf30: ErfFile30,
}

Format spec originally from the [DAToolset wiki](http://datoolset.net/wiki/ERF#File_Format),
with some updates and clarification. All values are little-endian.


## ERF v2.0

| Name              | Type     | Info                                     |
|-------------------|----------|------------------------------------------|
| magic             | utf16[8] | "ERF V2.0", a total of 16 bytes          |
| fileCount         | uint32   | Number of files encapsulated in this ERF |
| year              | uint32   | Year since 1900                          |
| day               | uint32   | Day since January 1st                    |
| unknown           | uint32   | Always 0xFFFFFFFF?                       |
| Table of Contents | [tocEntry](#tocentry-v2-0)[fileCount] |              |


### tocEntry v2.0

| Name   | Type      | Info                                         |
|--------|-----------|----------------------------------------------|
| name   | utf16[32] | Entry filename                               |	
| offset | uint32    | Offset to entry file from start of ERF file  |
| size   | uint32    | Length of entry file                         |


## ERF v2.2

This version introduces support for encrypted and compressed file data.

| Name              | Type     | Info                                                |
|-------------------|----------|-----------------------------------------------------|
| magic             | utf16[8] | "ERF V2.2", a total of 16 bytes                     |
| fileCount         | uint32   | Number of files encapsulated in this ERF            |
| year              | uint32   | Year since 1900                                     |
| day               | uint32   | Day since January 1st                               |
| unknown           | uint32   | Always 0xFFFFFFFF?                                  |
| [flags](#flags)   | uint32   |                                                     |
| moduleId          | uint32   | Used to look up password when encryption is present |
| passwordDigest    | byte[16] | MD5 sum of password (as a string)                   |
| Table of Contents | [tocEntry](#tocentry-v2-2)[fileCount] |                         |


### tocEntry v2.2

| Name         | Type      |                                                       |
|--------------|-----------|-------------------------------------------------------|
| name         | utf16[32] | Entry filename                                        |
| offset       | uint32    | Offset to entry file from start of ERF file           |
| packedSize   | uint32    | Packed length of entry file (including padding bytes) |
| unpackedSize | uint32    | Unpacked length of entry file                         |


## ERF v3.0

Found in Dragon Age 2.

This version introduces hashed file names (and extensions) for faster file lookup.

| Name              | Type     | Info                                                |
|-------------------|----------|-----------------------------------------------------|
| magic             | utf16[8] | "ERF V3.0", a total of 16 bytes                     |
| stringTableSize   | uint32   | Size of the file name string table                  |
| fileCount         | uint32   | Number of files encapsulated in this ERF            |
| [flags](#flags)   | uint32   |                                                     |
| moduleId          | uint32   | Used to look up password when encryption is present |
| passwordDigest    | byte[16] | MD5 sum of password (as a string)                   |
| stringTable       | char[stringTableSize][32] | 32-byte null-padded ASCII strings referenced by nameOffset in TOC entries |
| Table of Contents | [tocEntry](#tocentry-v3-0)[fileCount] |                         |


### tocEntry v3.0

| Name         | Type   | Info                                                             |
|--------------|--------|------------------------------------------------------------------|
| nameOffset   | int32  | Offset in stringTable of the entry filename, or -1 if none       |
| nameHash     | uint64 | FNV64 hash of lowercased file name, including path and extension |
| typeHash     | uint32 | FNV32 hash of lowercased file extension                          |
| offset       | uint32 | Offset to entry file, from start of ERF file                     |
| packedSize   | uint32 | Packed length of entry file (including padding bytes)            |
| unpackedSize | uint32 | Unpacked length of entry file                                    |


## Flags:

 0x00000001 (bit 0) : Protection flag

	Enforces password on unencrypted files in the toolset's ErfEditor

 0x000000F0 (bits 4 - 7) : Encryption scheme
```
(flags >> 4) & 0xF
 0 : None
 1 : XOR
 2 : Blowfish with 8-digit password
 3 : Blowfish with alternate password scheme (Dragon Age 2 only)
 Any other value is invalid.
```

0xE0000000 (bits 29 - 31) : Compression scheme
```
(flags >> 29) & 0x7
 0 : None
 1 : Bioware Zlib (Dragon Age 2 only)
 2 : Unknown (Dragon Age 2 only)
 3 : Unknown (Dragon Age 2 only)
 7 : Headerless Zlib (Dragon Age 2 only)
 Any other value is invalid.
```

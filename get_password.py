"""Recover passwords for Dragon Age: Origins ERF files.

Uses a fairly naive brute-force approach, but only takes about one minute to
complete due to the key space.
"""
"""
Key space:

Dragon Age: Origins passwords are 8-digit decimal integers. The ERF header
contains the md5 hash of the decimal ASCII string representation of that
number.

Dragon Age II passwords are... some other sort of thing. Based on the pyGFF
source code, The ERF header contains the md5 hash of the ciphertext that
results from that password being used to encrypt a bytearray containing
0x00 to 0xFF (in order) using Blowfish ECB.
"""

import os
import logging
from hashlib import md5
from multiprocessing import Pool
from typing import Any, Dict, Iterable

import utils
from erfdata.base import ErfFile, EncryptionScheme


_log = logging.getLogger(__name__)


def _bruteforce_passwords(
		digests: Iterable[bytearray],
		lower: int,
		upper: int,
		) -> dict:
	"""Brute-force solve for passwords in DAO style for a range.

	Counts from lower to upper (inclusive) and attempts every value.

	digests: The md5 digests to check for matches, as bytes.

	lower: The initial value to check.

	upper: The final value to check.
	"""
	matched = {}
	for i in range(lower, upper):
		guess = f'{i:08}'
		guess_md5 = md5(bytes(guess, 'ascii'))
		if guess_md5.digest() in digests:
			matched[guess_md5.hexdigest()] = guess
			digests.remove(guess_md5.digest())
			if len(digests) == 0:
				break
	return matched


def bruteforce_passwords(
		digests: Iterable[bytearray],
		lower : int = 0,
		upper: int = 1_0000_0000,
		singleprocess: bool = False,
		) -> Dict[str, str]:
	"""Brute-force solve for passwords in DAO style for a range.

	Counts from lower to upper (inclusive) and attempts every value.

	digests: The md5 digests to check for matches, as bytes.

	lower: The initial value to check.

	upper: The final value to check.

	singleprocess: If true, check all values in a single process (much slower).
	If false, divide the range into chunks and use a multiprocessing pool.
	"""
	out = {}

	if singleprocess:
		_log.debug("Single process enabled")
		return _bruteforce_passwords(digests, lower, upper)

	pool = Pool(maxtasksperchild=2)
	pool_results = []
	# this chunk size is decently light on memory
	chunk_size = upper // os.cpu_count() // 4
	for i in range(lower, upper, chunk_size):
		pool_results.append(pool.apply_async(func = _bruteforce_passwords, args=(digests, i, min(upper, i+chunk_size-1))))
	
	for result in pool_results:
		new_out = result.get()
		print_progress(len(new_out))
		out |= new_out
		if len(out) == len(digests):
			break
	
	pool.close()
	return out


def print_progress(found_count: Any) -> None:
	"""Append to progress bar.

	found_count: The value to print, should be 0-2 chars.
	"""
	if _log.getEffectiveLevel() > logging.INFO:
		return
	message = found_count or '.' # replace zeroes for aesthetic
	print(f"{message:<2}", end='', flush=True)


def main(erfs: Iterable[ErfFile], singleprocess: bool = False) -> None:
	unknown_digests = set()
	
	cache = utils.load_history()
	prev_history_len = len(cache)

	for erf in erfs:
		if erf.encryption_scheme != EncryptionScheme.NotEncrypted and not cache.get(erf.password_digest.hex()):
			unknown_digests.add(erf.password_digest)

	if len(unknown_digests) >= 1:
		_log.info(f"Recovering unknown passwords for digests: {[d.hex() for d in unknown_digests]}")
		cache |= bruteforce_passwords(unknown_digests, singleprocess=singleprocess)

	for erf in erfs:
		_log.info(f"File: {erf.path}")
		_log.info(f"\tEncryption: {erf.encryption_scheme.name}")
		if erf.encryption_scheme != EncryptionScheme.NotEncrypted:
			pw = cache.get(erf.password_digest.hex())
			_log.info(f"\tPassword:   {pw or 'Recovery failed'}")

	if len(cache) > prev_history_len:
		utils.save_history(cache)


if __name__ == "__main__":
	parser = utils.cmdline_parser
	parser.usage="%prog [OPTION]... PATH..."
	parser.description=(
		"Recover passwords for ERF files in PATH"
		"PATH may be a single file or a directory containing ERF files"
	)
	options, args = parser.parse_args()
	utils.config_cmdline_logger(options.log_level)
	if not args:
		parser.print_usage()
		exit(1)

	paths = set()
	for arg in args:
		paths.update(utils.get_files_in_path(arg, '.erf'))

	if len(paths) < 1:
		_log.error("No erfs found")
		exit(2)
	
	_log.info(f"Processing {len(paths)} erfs")

	main([ErfFile(p) for p in paths], singleprocess=options.singleprocess)

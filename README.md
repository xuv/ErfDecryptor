A Dragon Age: Origins modding tool to decrypt ERF files.


## Compatibility

This has been tested to work for all ERFs in Dragon Age: Origins Ultimate
Edition from GOG. Dragon Age 2 is unsupported but may be added in future.

DAO seems to run fine with all ERFs decrypted by this script, and takes less 
than half the time to load the world than with encrypted files. If this script
causes any issues, please let me know!


## Requirements

If you only want passwords to use manually, you need Python 3.9 (lower versions may also work).
If you also want to decrypt the files you'll need [pycryptodomex](https://github.com/Legrandin/pycryptodome).


```
pip install pycryptodomex
```

If you prefer `pycryptodome` instead, just change the import in `decrypt.py`,
the syntax is otherwise the same.


## Usage

Pass the `--help` flag to either script for all options, but most commonly:


1. Recover the passwords for all encrypted ERFs.

	This saves all passwords to `erf_passwords.txt`, and takes about a minute.

```
python get_password.py "path\to\Dragon Age Origins\Addins"
```

2. Decrypt all encrypted ERFs.

	This requires the password file generated by `get_password.py`. The
	original files will be backed up then replaced with the decrypted versions.
	This takes about 15 seconds.

```
python decrypt.py "path\to\Dragon Age Origins\Addins"
```

And you're done!

Alternatively, if you don't want to directly decrypt your files, the
`erf_passwords.txt` file produced by `get_password.py` maps md5 hashes
from the file headers to their passwords. You can copy those passwords
into a tool like [pyGFF](https://www.nexusmods.com/dragonage/mods/4512).


## Legal

This tool is intended to aid in creating mods for legitimately obtained copies
of DLC. Be aware of your local laws about breaking such encryption before
downloading or using this tool.


## Credits

Many thanks to the people who reversed and catalogued the ERF format as
described on [the wiki](http://datoolset.net/wiki/ERF), and to Mephales
including their source code with pyGFF, which I only realised a few days after
starting this project.

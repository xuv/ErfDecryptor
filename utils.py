"""Shared setup and data that should probably have a better place."""

import logging
import os

from sys import stdout
from optparse import OptionGroup, OptionParser
from typing import Dict, Set


_log = logging.getLogger(__name__)

history_file = 'erf_passwords.txt'


# Command line options that are shared between all tools
cmdline_parser = OptionParser()
cmdline_parser.add_option("-q", "--quiet",
	help="suppress progress console output",
	action="store_const",
	const=logging.ERROR,
	dest="log_level",
)
_debug_group = OptionGroup(cmdline_parser, "Debugging")
_debug_group.add_option("--debug",
	help="print debug messages",
	action="store_const",
	const=logging.DEBUG,
	dest="log_level",
)
_debug_group.add_option("--singleprocess",
	help="disable multiprocessing",
	action="store_true",
)
cmdline_parser.add_option_group(_debug_group)


def load_history() -> Dict[str, str]:
	"""Get hash-to-password dict from file."""
	if not os.path.exists(history_file):
		return {}

	history = {}
	with open(history_file, 'r') as file:
		for line in file:
			digesthex, password = line[:-1].split('=') # omit trailing newline
			history[digesthex] = password
	return history


def save_history(passwords: dict) -> None:
	"""Save hash-to-password dict to file."""
	with open(history_file, 'w') as file:
		for digest, password in passwords.items():
			file.write(f"{digest}={password}\n")
		_log.info(f"Saved new digest passwords to {history_file}")


def get_files_in_path(path: str, extension: str) -> Set[str]:
	"""Get the paths of all files in path with the given extension, recursively.
	
	path: If a file, check only that file. If a directory, recursively check
	all files below it.

	extension: The extension to match.
	"""
	_log.debug(f"Searching for *{extension} in {path}")
	matches = set()
	extension_index = -1 * len(extension)

	if os.path.isfile(path) and path[extension_index:].lower() == extension:
		matches.add(path)

	for dirpath, dirnames, filenames in os.walk(path, followlinks=False):
		for name in filenames:
			if name[extension_index:].lower() == extension:
				matches.add(os.path.join(dirpath, name))
	return matches


def config_cmdline_logger(level: int) -> None:
	"""Configure global logger format for CLI.
	
	level: The logging level to set.
	"""
	logging.basicConfig(
		stream=stdout,
		format="{message}",
		style='{',
		level=(level or logging.INFO),
	)

"""Decryption tool for ERF files."""

import os
import logging

from typing import Iterable
from multiprocessing import Pool
from Cryptodome.Cipher import Blowfish

import utils
from erfdata.base import ErfFile, EncryptionScheme


_log = logging.getLogger(__name__)


def decrypt_blowfish(erf: ErfFile) -> ErfFile:
	"""Decrypt an ErfFile using Blowfish ECB and return it.

	erf: An encrypted ErfFile.
	"""
	if not erf.has_valid_password():
		raise ValueError("Incorrect password", erf.password, erf.password_digest.hex())
	cipher = Blowfish.new(erf.password, Blowfish.MODE_ECB)
	for offset, data in erf.contents.items():
		try:
			erf.contents[offset] = cipher.decrypt(data)
		except Exception as e:
			e.args += (erf.path,)
			raise e
	erf.encryption_scheme = EncryptionScheme.NotEncrypted
	return erf


def decrypt_xor(erf: ErfFile) -> ErfFile:
	"""Decrypt an ErfFile using XOR and return it.

	erf: An encrypted ErfFile.
	"""
	# Seems to only be used in pre-DAO files?
	raise NotImplementedError


def decrypt(erfs: Iterable[ErfFile], noreplace: bool = False, singleprocess: bool = False) -> None:
	"""Decrypt ErfFiles and save them to disk.

	erfs: ErfFiles to decrypt. Any unencrypted items will be skipped.

	noreplace: If true, save decrypted copies to an adjacent file. Otherwise,
	create a backup of the original then replace it with the decrypted version.

	singleprocess: If true, disable multiprocess decryption. Mostly intended
	for debugging.
	"""
	"""
	This saves directly to disk instead of returning the collection in order
	to reduce memory usage. This may change if this tool is expanded to other
	purposes.
	"""
	cache = utils.load_history()
	needs_blowfish = []
	needs_xor = []

	for erf in erfs:
		if erf.encryption_scheme == EncryptionScheme.NotEncrypted:
			continue

		try:
			erf.password = cache[erf.password_digest.hex()]
		except:
			_log.warning(f"No password for file (run get_passwords.py on it first): {erf.path}")
			continue

		if erf.encryption_scheme == EncryptionScheme.XOR:
			needs_xor.append(erf)
		else:
			needs_blowfish.append(erf)

	pool = Pool()

	total = len(needs_blowfish)
	done = 0
	_log.info(f"Decrypting {total} files with Blowfish.")
	if singleprocess:
		_log.debug("Single process enabled")
		results = [decrypt_blowfish(erf) for erf in needs_blowfish]
	else:
		results = pool.imap_unordered(decrypt_blowfish, needs_blowfish)

	for erf in results:
		save_decrypted(erf, noreplace)
		done += 1
		_log.info(f"[{done:3}/{total:3}] Decrypted: {erf.path}")
	
	total = len(needs_xor)
	done = 0
	_log.info(f"Decrypting {len(needs_xor)} files with XOR.")
	results = pool.imap_unordered(decrypt_xor, needs_xor)
	for erf in results:
		save_decrypted(erf, noreplace)
		_log.info(f"[{done:3}/{total:3}] Decrypted: {erf.path}")
	
	pool.close()


def save_decrypted(erf: ErfFile, noreplace: bool = False) -> None:
	"""Save an ErfFile to disk.
	
	erf: The ErfFile to save. Its `path` attribute must be a filepath to
	its original filename.

	noreplace: If true, save to erf.path but replace the extension with
	`.decrypted.erf`. Otherwise, backup the original file and save directly
	to erf.path.
	"""
	target_extension = '.decrypted.erf' if noreplace else '.erf'
	target_path = erf.path[:-4] + target_extension
	bak_path = target_path + '.bak'
	# create backup if target exists and no backup is already present
	try:
		os.rename(target_path, bak_path)
	except:
		pass
	with open(target_path, 'wb') as file:
		erf.write_bytes(file)


if __name__ == "__main__":	
	parser = utils.cmdline_parser
	parser.usage = "%prog [OPTION]... PATH..."
	parser.description = ("Decrypt any ERF files in PATH"
		"PATH: Path to an ERF file or a directory containing ERF files."
	)
	parser.add_option("--noreplace",
		help="Save decrypted files to *.decrypted.erf instead of replacing the original",
		action="store_true",
	)

	options, args = parser.parse_args()
	utils.config_cmdline_logger(options.log_level)
	if not args:
		parser.print_usage()
		exit(1)

	paths = set()
	for arg in args:
		paths.update(utils.get_files_in_path(arg, '.erf'))

	if len(paths) < 1:
		_log.error("No erfs found")
		exit(2)
	
	_log.info(f"Processing {len(paths)} erfs.")

	decrypt([ErfFile(p) for p in paths], options.noreplace, options.singleprocess)
